const defaultState = {
    task: [],
    val: '未着手',
    val1: '未着手',
    val2: '未着手',
    val3: '未着手',
    val4: '未着手',
    val5: '未着手',
    val6: '未着手',
    val7: '未着手',
    val8: '未着手',
    val9: '未着手',
    val10: '未着手'
}
export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'UPDATE_TASK':
            return {
                ...state,
                task: action.task
            }
        case 'UPDATE_VAL':
            return {
                ...state,
                val: action.val
            }
        case 'UPDATE_VAL1':
            return {
                ...state,
                val1: action.val1
            }
        case 'UPDATE_VAL2':
            return {
                ...state,
                val2: action.val2
            }
        case 'UPDATE_VAL3':
            return {
                ...state,
                val3: action.val3
            }
        case 'UPDATE_VAL4':
            return {
                ...state,
                val4: action.val4
            }
        case 'UPDATE_VAL5':
            return {
                ...state,
                val5: action.val5
            }
        case 'UPDATE_VAL6':
            return {
                ...state,
                val6: action.val6
            }
        case 'UPDATE_VAL7':
            return {
                ...state,
                val7: action.val7
            }
        case 'UPDATE_VAL8':
            return {
                ...state,
                val8: action.val8
            }
        case 'UPDATE_VAL9':
            return {
                ...state,
                val9: action.val9
            }
        case 'UPDATE_VAL10':
            return {
                ...state,
                val10: action.val10
            }
        default:
            return state;
    }
}
