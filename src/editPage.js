import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import 'whatwg-fetch';
import * as actions from './action/actions';
export class EditPage_Comp extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch);
        this.state = {
            id: '',
            val: ''
        }
    }

    render() {

        const [val, setVal] = React.useState('cat');
      
        const handleChange = e => setVal(e.target.value);

        return (
            <div>
            <label>
              <input
                type="radio"
                value="cat"
                onChange={handleChange}
                checked={val === 'cat'}
              />
              猫派
            </label>
            <label>
              <input
                type="radio"
                value="dog"
                onChange={handleChange}
                checked={val === 'dog'}
              />
              犬派
            </label>
            <p>選択値：{val}</p>
            </div>
        )
    }
}
EditPage_Comp.propTypes = {
    dispatch: PropTypes.func,
    history: PropTypes.object
}
function mapStateToProps(state) {
    return state
}
export const EditPage = withRouter(connect(mapStateToProps)(EditPage_Comp))