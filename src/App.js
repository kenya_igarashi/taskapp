import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { connect } from 'react-redux'
import {TopPage} from './topPage.js'
import {EditPage} from './editPage.js'
class App extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path="/" component={TopPage} />
                    <Route path="/edit" component={EditPage} />
                </Switch>
            </Router>
        )
    }
}
function mapStateToProps(state) {
    return state
}
export default connect(mapStateToProps)(App)