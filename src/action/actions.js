export function updateTask(json) {
    return {
        type: 'UPDATE_TASK',
        task: json
    }
}
export function updateVal(val) {
    return {
        type: 'UPDATE_VAL',
        val: val
    }
}
export function updateVal1(val1) {
    return {
        type: 'UPDATE_VAL1',
        val1: val1
    }
}
export function updateVal2(val2) {
    return {
        type: 'UPDATE_VAL2',
        val2: val2
    }
}
export function updateVal3(val3) {
    return {
        type: 'UPDATE_VAL3',
        val3: val3
    }
}
export function updateVal4(val4) {
    return {
        type: 'UPDATE_VAL4',
        val4: val4
    }
}
export function updateVal5(val5) {
    return {
        type: 'UPDATE_VAL5',
        val5: val5
    }
}
export function updateVal6(val6) {
    return {
        type: 'UPDATE_VAL6',
        val6: val6
    }
}
export function updateVal7(val7) {
    return {
        type: 'UPDATE_VAL7',
        val7: val7
    }
}
export function updateVal8(val8) {
    return {
        type: 'UPDATE_VAL8',
        val8: val8
    }
}
export function updateVal9(val9) {
    return {
        type: 'UPDATE_VAL9',
        val9: val9
    }
}
export function updateVal10(val10) {
    return {
        type: 'UPDATE_VAL10',
        val10: val10
    }
}