import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import * as actions from './action/actions';
import moment from 'moment'
import Modal from "react-modal";

Modal.setAppElement("#root");

class TopPage_Comp extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch);
        this.state = {
            id: '',
            task: '',
            dueDate: '',
            delFlg: '',
            val: '未着手',
            deleteId: 0,
            editTask: '',
            editDueDate: '',
            date: new Date(),
            progress: [
            ]
        }
    }

    componentDidMount() {
        fetch("http://localhost:8080/task/getTask", {
            method: "GET"
        })
            .then((response) => {
                response.json().
                    then(json => {
                        this.action.updateTask(json)
                    })
            })
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }
    tick() {
        this.setState({
            date: new Date()
        });
    }

    handleClick(id) {
        confirm("削除してもよろしいですか？") &&
            this.deleteTask(id);
    }
    deleteTask(id) {
        // stateの値をJSONに整形
        // POST送信実⾏
        fetch("http://localhost:8080/task/deleteTask/" + id, {
            method: "DELETE",
        })
            // レスポンス返却時の処理
            .then((response) => {
                if (response.status === 200) {
                    fetch("http://localhost:8080/task/getTask", {
                        method: "GET"
                    })
                        .then((response) => {
                            response.json().
                                then(json => {
                                    this.action.updateTask(json)
                                })
                        })
                    this.props.history.push("/")
                } else {
                    alert("削除に失敗しました。")
                }
            })
            .catch(error => console.error(error));
    }

    editButtonClicked(id) {
        if(window.confirm("タスクを更新します。よろしいですか？")){
            this.setState({ id: id });
            this.editSend(id);
        }
    }
    editSend(id) {
        // 空欄バリデーション
          if(this.state.editTask === ''){
              alert("タスクを入力してください")
             return
        }
        if(this.state.editDueDate === ''){
            alert("日付を入力してください")
           return
        }
        // stateの値をJSONに整形
        const data = { id: id, task: this.state.editTask, dueDate: this.state.editDueDate }
        // POST送信実⾏
        fetch("http://localhost:8080/task/editTask/" + id, {
            method: "POST",
            body: JSON.stringify(data)
        })
            // レスポンス返却時の処理
            .then((response) => {
                if (response.status === 200) {
                    fetch("http://localhost:8080/task/getTask", {
                        method: "GET"
                    })
                        .then((response) => {
                            response.json().
                                then(json => {
                                    this.action.updateTask(json)
                                })
                        })
                    this.props.history.push("/")
                } else {
                    alert("編集に失敗しました。")
                }
            })
            .catch(error => console.error(error));
    }

    resetButtonChecked(id,task,dueDate) {
        if(window.confirm("タスクを削除します。よろしいですか？")){
            this.resetButtonClicked(id,task,dueDate);
        }
    }

    resetButtonClicked(id,task,dueDate) {
         // stateの値をJSONに整形
        const data = { id: id, task: task, dueDate: dueDate}
        // POST送信実⾏
        fetch("http://localhost:8080/task/deleteTask/" + id, {
            method: "POST",
            body: JSON.stringify(data)
        })
            // レスポンス返却時の処理
            .then((response) => {
                if (response.status === 200) {
                    fetch("http://localhost:8080/task/getTask", {
                        method: "GET"
                    })
                        .then((response) => {
                            response.json().
                                then(json => {
                                    this.action.updateTask(json)
                                })
                        })
                    this.props.history.push("/")
                } else {
                    alert("削除に失敗しました。")
                }
            })
            .catch(error => console.error(error));
    }

    progressHandleChange(e) {
        const progress_copy = this.state.progress.slice();
        progress_copy[e.target.name - 1] = e.target.value
        this.setState({ progress: progress_copy })
    }

    limitDate(date1) {
        var nowDate = new Date();
        var dnumNow = nowDate.getTime();

        var targetDate = new Date(date1);
        var dnumTarget = targetDate.getTime();

        var diffMSec = dnumTarget - dnumNow;
        var diffDays = diffMSec / ( 1000 * 60 * 60 * 24 );
        var showDays = Math.ceil( diffDays );

        return showDays
    }

    lowerThanDateOnly(date1) {
        var date = new Date()
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();

        var year1 = date1.slice(0, 4);
        var month1 = date1.slice(5, 7);
        var day1 = date1.slice(8, 10);

        if (year == year1) {
            if (month == month1) {
                return day <= day1;
            }
            else {
                return month < month1;
            }
        } else {
            return year < year1;
        }
    }

    taskStart(id,task,dueDate) {
        this.taskProgress(id,task,dueDate,'作業中');
    }
    taskHold(id,task,dueDate) {
        this.taskProgress(id,task,dueDate,'保留中');
    }
    taskEnd(id,task,dueDate) {
        this.taskProgress(id,task,dueDate,'完了');
    }
    taskProgress(id,task,dueDate,prog){
                // stateの値をJSONに整形
                const data = { id: id, task: task, dueDate: dueDate, progress: prog}
                // POST送信実⾏
                fetch("http://localhost:8080/task/progress/" + id, {
                    method: "POST",
                    body: JSON.stringify(data)
                })
                    // レスポンス返却時の処理
                    .then((response) => {
                        if (response.status === 200) {
                            fetch("http://localhost:8080/task/getTask", {
                                method: "GET"
                            })
                                .then((response) => {
                                    response.json().
                                        then(json => {
                                            this.action.updateTask(json)
                                        })
                                })
                            this.props.history.push("/")
                        } else {
                            alert("編集に失敗しました。")
                        }
                    })
                    .catch(error => console.error(error));
    }

    render() {

        const handleChaged = (e) => {
            this.setState({ [e.target.name]: e.target.value })
        }

        const taskList = this.props.task.map((t) => (   
            <table border='1' key={t.id} className="table">
                <tr className="tr">
                    <th className="task">内容</th>
                    {this.limitDate(t.dueDate) < 0 && <th className="danger">({-this.limitDate(t.dueDate)} 日超過) </th>}
                    {this.limitDate(t.dueDate) >= 0 && <th className="due">(あと {this.limitDate(t.dueDate)} 日) </th>}
                    <input type='hidden' value={t.id}></input>
                </tr>
                <tr className="tr" >
                    <td>{t.task}</td>
                    {this.lowerThanDateOnly(moment(t.dueDate).format('yyyy-MM-DD')) && <td>{moment(t.dueDate).format('yyyy-MM-DD')}</td>}
                    {!this.lowerThanDateOnly(moment(t.dueDate).format('yyyy-MM-DD')) && <td className="danger">{moment(t.dueDate).format('yyyy-MM-DD')}</td>}
                    <input type='hidden' value={t.id}></input>
                </tr>
                <tr className="tr">
                    <td>
                    <button className="progressButton" onClick={() => this.taskStart(t.id,t.task,t.dueDate)}>着手</button>　　　　　
                    <button className="progressButton" onClick={() => this.taskHold(t.id,t.task,t.dueDate)}>保留</button>　　　　　
                    <button className="progressButton" onClick={() => this.taskEnd(t.id,t.task,t.dueDate)}>完了</button>
                    </td>
                    <td className="now" name={"val" + t.id}>
                        現在:{t.progress}
                    </td>
                </tr>
                <tr className="edit">
                    <td>編集</td>
                    <td><button onClick={() => this.editButtonClicked(t.id)}>編集</button>　
                    <button onClick={() => this.resetButtonChecked(t.id,t.task,t.dueDate)}>削除</button>
                    </td>
                </tr>
                <tr className="tr">
                    <td><textarea className="textarea" rows={3} cols={60} name="editTask" onChange={handleChaged} input type="text" placeholder={t.task}></textarea></td>
                    <td><input type="date" name="editDueDate" onChange={handleChaged}></input></td>
                    <input type='hidden' name="id" value={t.id}></input>
                </tr>
            </table>
        ))
        const sendButtonClicked = () => {
            postSend()
        }
        const postSend = () => {
            // 空欄バリデーション
            if(this.state.task === ''){
                alert("タスクを入力してください")
                return
            }
            if(this.state.dueDate === ''){
                alert("日付を入力してください")
                return
            }
            // stateの値をJSONに整形
            const data = { task: this.state.task, dueDate: this.state.dueDate }
            // POST送信実⾏
            fetch("http://localhost:8080/task/postTask", {
                method: "POST",
                body: JSON.stringify(data)
            })
                // レスポンス返却時の処理
                .then((response) => {
                    if (response.status === 200) {
                        fetch("http://localhost:8080/task/getTask", {
                            method: "GET"
                        })
                            .then((response) => {
                                response.json().
                                    then(json => {
                                        console.log(json)
                                        this.action.updateTask(json)
                                    })
                            })
                        this.props.history.push("/")
                    } else {
                        alert("追加に失敗しました。")
                    }
                })
                .catch(error => console.error(error));
        }

        return (
            <div className="page">
                <h1 className="page-title">タスクリスト</h1>
                <div>現在時刻: {this.state.date.toLocaleTimeString()}</div>
                <div className="task-list">
                    {taskList}
                </div>
                <br />
                <a className="edit">新規タスク</a>
                <div className="postArea">
                    <textarea className="textarea" rows={5} cols={80} name="task" onChange={handleChaged}></textarea>
                    <br />
                    <input type="date" name="dueDate" onChange={handleChaged}></input>
                </div>
                <p></p>
                <button onClick={sendButtonClicked}>追加</button>
                <p>© IgarashiKenya 2021</p>
            </div>
        )
    }
}
TopPage_Comp.propTypes = {
    dispatch: PropTypes.func,
    task: PropTypes.object,
    history: PropTypes.object,
    val: PropTypes.string
}
function mapStateToProps(state) {
    return state
}
export const TopPage = withRouter(connect(mapStateToProps)(TopPage_Comp))
